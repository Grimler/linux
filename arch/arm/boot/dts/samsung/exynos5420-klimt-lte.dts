// SPDX-License-Identifier: GPL-2.0
/*
 * Samsung's Exynos5420 Klimt LTE board device tree source
 *
 * Copyright (c) 2012-2013 Samsung Electronics Co., Ltd.
 *		http://www.samsung.com
 * Copyright (c) 2022 Henrik Grimler
 */

/dts-v1/;
#include "exynos5420-galaxy-tab-common.dtsi"

/ {
	model = "Samsung Klimt LTE based on Exynos5420";
	compatible = "samsung,klimt-lte", "samsung,exynos5420", \
		     "samsung,exynos5";
};

&ldo15_reg {
	regulator-name = "VDD_LEDA_2V8";
	regulator-min-microvolt = <2800000>;
	regulator-max-microvolt = <2800000>;

	regulator-state-mem {
		regulator-on-in-suspend;
	};
};

&ldo17_reg {
	regulator-name = "VDD_VCI_3V0";
	regulator-min-microvolt = <3000000>;
	regulator-max-microvolt = <3000000>;

	regulator-state-mem {
		regulator-off-in-suspend;
	};
};

&ldo28_reg {
	regulator-name = "VDD3_1V8";
	regulator-min-microvolt = <1800000>;
	regulator-max-microvolt = <1800000>;

	regulator-state-mem {
		regulator-off-in-suspend;
	};
};

&ldo29_reg {
	regulator-name = "VDDR_1V6";
	regulator-min-microvolt = <1600000>;
	regulator-max-microvolt = <1600000>;

	regulator-state-mem {
		regulator-off-in-suspend;
	};
};

&ldo31_reg {
	regulator-name = "VDD_GRIP_1V8";
	regulator-min-microvolt = <1800000>;
	regulator-max-microvolt = <1800000>;
	regulator-always-on;
	regulator-boot-on;

	regulator-state-mem {
		regulator-off-in-suspend;
	};
};

&ldo32_reg {
	regulator-name = "VDD_TSP_1V8";
	regulator-min-microvolt = <1800000>;
	regulator-max-microvolt = <1800000>;

	regulator-state-mem {
		regulator-off-in-suspend;
	};
};

&mmc_2 {
	sd-uhs-sdr104;
};
